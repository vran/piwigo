---

# address on which to accept fastcgi requests
web_php_listen_address: "127.0.0.1:9000"

#################################### nginx ####################################
# nginx user and group
nginx_user: "{{ project_www_user }}"
nginx_group: "{{ project_www_group }}"

# path to nginx default site config template file
nginx_default_template: "inventories/prod/templates/nginx/default.j2"

# path to nginx default root directory
nginx_default_root: "/var/www/html"

# locations in nginx default site
nginx_default_locations:
  - name: "/"
    parameters: "try_files $uri $uri/ =404;"
  - name: "/phpmyadmin"
    parameters: |
      root /var/www/html/;
      index index.php index.html index.htm;
  - name: "/{{ project_name }}"
    parameters: |
      root /var/www/html/;
      index index.php index.html index.htm;
  - name: "/stub_status"
    parameters: |
      stub_status on;
      access_log off;
  - name: "~ \\.php$"
    parameters: |
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
              fastcgi_split_path_info "^(.+?\\.php)(/.*)$";
              fastcgi_pass {{ web_php_listen_address }};
              include fastcgi_params;
              if (!-f $document_root$fastcgi_script_name) {
                  return 404;
              }

##################################### php #####################################
# set php version to be installed
php_version: "7.4"

# set to "true" to remove currently installed php version
php_remove_old_version: true

# select the php extensions to be installed
php_extensions:
  - "php-mbstring"
  - "php-fpm"
  - "php-memcached"
  - "php-mysql"
  - "php-json"
  - "php-zip"
  - "php-xml"

# set which web server is used: "apache2" or "nginx"
php_webserver: "{{ project_webserver }}"

# path to php-fpm.conf template file
php_fpm_pools_template: "inventories/prod/templates/php/pools.j2"

# pool configurations
php_fpm_pools:
  - name: "phpmyadmin"
    user: "{{ project_www_user }}"
    group: "{{ project_www_group }}"
    listen_address: "{{ web_php_listen_address }}"
    listen_owner: "{{ project_www_user }}"
    listen_group: "{{ project_www_group }}"
    listen_mode: 0660
    listen_allowed_clients: "127.0.0.1"
    pm: "dynamic"
    pm_max_children: 50
    pm_start_servers: 20
    pm_min_spare_servers: 10
    pm_max_spare_servers: 30
    pm_process_idle_timeout: "10s"
    pm_max_requests: 5000
    pm_status_path: "/phpmyadmin"

################################# phpmyadmin ##################################
# list of phpmyadmin requirement packages
phpmyadmin_requirements: {}

# webserver’s user and group names
phpmyadmin_user: "{{ project_www_user }}"
phpmyadmin_group: "{{ project_www_group }}"

# choose webserver to use: "apache2" or "nginx"
phpmyadmin_webserver: "{{ project_webserver }}"

# default behavior for JavaScript error reporting: "ask", "always" or "never"
phpmyadmin_send_error_reports: "never"

# path to config.inc.php template file
phpmyadmin_config_inc_php_template: "inventories/prod/templates/phpmyadmin/config.inc.php.j2"

# user name for mysql
phpmyadmin_mysql_root_user: "{{ project_mysql_root_user }}"

# password for mysql. hide it in production!
phpmyadmin_mysql_root_password: "{{ project_mysql_root_password }}"

# user name for mysql
phpmyadmin_mysql_user: "{{ project_phpmyadmin_db_user }}"

# password for mysql. hide it in production!
phpmyadmin_mysql_pass: "{{ project_phpmyadmin_db_password }}"

# mysql server hostname
phpmyadmin_mysql_host: "{{ hostvars[groups.db_master.0].ansible_host }}"

# mysql port
phpmyadmin_mysql_port: "{{ project_mysql_port }}"

# mysql database name
phpmyadmin_mysql_db_name: "{{ project_phpmyadmin_db_name }}"

################################# application #################################
# application directory path
app_application_path: "/piwigo"

# application download address
app_download_url: "https://piwigo.org/download/dlcounter.php?code=latest"

# path to application config template
app_config_template_path: "inventories/prod/templates/piwigo/install.php.j2"

# application administrator name
app_admin_name: "piwigo_dev"

# application administrator email address
app_admin_mail: "piwigo.dev@mail.io"

# "true" to subscribe
app_is_newsletter_subscribe: false
