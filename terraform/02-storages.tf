# create storages

# resource "yandex_storage_bucket" "s3_tfstate" {
#   access_key = yandex_iam_service_account_static_access_key.sa_static_key.access_key
#   secret_key = yandex_iam_service_account_static_access_key.sa_static_key.secret_key
#   bucket     = var.tfstate_bucket_name
# }

resource "yandex_compute_filesystem" "db_filesystem" {
  count = var.db_hosts_number
  name  = "${var.db_filesystem_name}-${count.index + 1}"
  type  = var.db_filesystem_type
  zone  = var.availibility_zone_a
  size  = var.db_filesystem_size
  lifecycle {
    prevent_destroy = true
  }
}