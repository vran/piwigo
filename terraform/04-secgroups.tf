resource "yandex_vpc_security_group" "secgroup_web" {
  name        = "secgroup-web"
  network_id  = yandex_vpc_network.network_a.id
# allow internal traffic
  ingress {
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  egress {
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
    }
# allow traffic for "secgroup-db"
  ingress {
    protocol          = "ANY"
    security_group_id = yandex_vpc_security_group.secgroup_db.id
    from_port         = 0
    to_port           = 65535
  }
  egress {
    protocol          = "ANY"
    security_group_id = yandex_vpc_security_group.secgroup_db.id
    from_port         = 0
    to_port           = 65535
    }
# allow whitelist traffic
  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = var.whitelist_ip
    from_port      = var.application_port
    to_port        = var.application_port
  }
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = var.whitelist_ip
    from_port      = var.application_port
    to_port        = var.application_port
  }
  dynamic "egress" {
    for_each = var.monitoring_ports
    content {
      protocol       = "TCP"
      v4_cidr_blocks = var.whitelist_ip
      from_port      = egress.value
      to_port        = egress.value
    }
  }
# allow ssh connection
  ingress {
    protocol       = "TCP"
    from_port      = var.management_port_external
    to_port        = var.management_port_internal
    v4_cidr_blocks = [var.management_ip]
  }
# open outgoing traffic on port 80 to use apt-get
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 80
    to_port        = 80
  }
# open outgoing traffic on port 443 to use pip
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 443
    to_port        = 443
  }
}

resource "yandex_vpc_security_group" "secgroup_db" {
  name        = "secgroup-db"
  network_id  = yandex_vpc_network.network_a.id
# allow internal traffic
  ingress {
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  egress {
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
    }
# allow outgoing whitelist traffic
  dynamic "egress" {
    for_each = var.monitoring_ports
    content {
      protocol       = "TCP"
      v4_cidr_blocks = var.whitelist_ip
      from_port      = egress.value
      to_port        = egress.value
    }
  }
# allow ssh connection
  ingress {
    protocol       = "TCP"
    from_port      = var.management_port_external
    to_port        = var.management_port_internal
    v4_cidr_blocks = [var.management_ip]
  }
# open outgoing traffic on port 80 to use apt-get
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 80
    to_port        = 80
  }
# open outgoing traffic on port 443 to use pip
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 443
    to_port        = 443
  }
}