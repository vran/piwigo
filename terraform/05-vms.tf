# create virtual maschines
# create a hosts file for deployment using ansibe

resource "null_resource" "db_hosts_string" {
  provisioner "local-exec" {
    command = "echo [db] > ${var.hosts_path}"
  }
}

resource "yandex_compute_instance" "db_hosts" {
  count       = var.db_hosts_number
  name        = "prod-db${count.index + 1}"
  hostname    = "prod-db${count.index + 1}"
  platform_id = var.platform_id_name
  resources {
    core_fraction = var.db_hosts_core_fraction
    cores         = var.db_hosts_cpu
    memory        = var.db_hosts_ram
  }
  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.operating_system_id
      type     = "network-hdd"
      size     = var.db_hosts_disk_size
    }
  }
  filesystem {
    filesystem_id = yandex_compute_filesystem.db_filesystem[count.index].id
  }
  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet_a.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.secgroup_db.id]
  }
  metadata   = {
    user-data = templatefile("./meta.yml", { username = var.ssh_user, ssh_key = "${local.ssh_key_pub}" })
    ssh-keys = "${var.ssh_user}:${local.ssh_key_pub}"
  }
  provisioner "local-exec" {
    command  = "echo ${self.name} ansible_host=${self.network_interface.0.nat_ip_address} >> ${var.hosts_path}"
  }
  connection {
    type        = "ssh"
    host        = self.network_interface.0.nat_ip_address
    user        = var.ssh_user
    private_key = local.ssh_key
    timeout     = "1m"
  }
  provisioner "remote-exec" {
    inline  = [
      "sudo mkdir /var/lib/mysql",
      "sudo mount -t virtiofs ${yandex_compute_filesystem.db_filesystem[count.index].id} /var/lib/mysql",
      "echo '${var.db_filesystem_name}-${count.index + 1} /var/lib/mysql virtiofs rw 0 0' | sudo tee --append /etc/fstab"
    ]
  }
  depends_on = [
    null_resource.db_hosts_string,
    yandex_compute_filesystem.db_filesystem
  ]
}

resource "null_resource" "web_hosts_string" {
  provisioner "local-exec" {
    command  = "echo [web] >> ${var.hosts_path}"
  }
  depends_on = [yandex_compute_instance.db_hosts]
}

resource "yandex_compute_instance" "web_hosts" {
  count       = var.web_hosts_number
  name        = "prod-web${count.index + 1}"
  hostname    = "prod-web${count.index + 1}"
  platform_id = var.platform_id_name
  resources {
    core_fraction = var.web_hosts_core_fraction
    cores         = var.web_hosts_cpu
    memory        = var.web_hosts_ram
  }
  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.operating_system_id
      type     = "network-hdd"
      size     = var.web_hosts_disk_size
    }
  }
  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet_a.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.secgroup_web.id]
  }
  metadata    = {
    user-data = templatefile("./meta.yml", { username = var.ssh_user, ssh_key = "${local.ssh_key_pub}" })
    ssh-keys  = "${var.ssh_user}:${local.ssh_key_pub}"
  }
  provisioner "local-exec" {
    command = "echo ${self.name} ansible_host=${self.network_interface.0.nat_ip_address} >> ${var.hosts_path}"
  }
  depends_on = [null_resource.web_hosts_string]
}

resource "null_resource" "db_hosts_string_1" {
  provisioner "local-exec" {
    command = "echo [db_master] >> ${var.hosts_path}"
  }
  depends_on = [yandex_compute_instance.web_hosts]
}
resource "null_resource" "db_hosts_string_2" {
  provisioner "local-exec" {
    command = "echo ${yandex_compute_instance.db_hosts.0.name} >> ${var.hosts_path}"
  }
  depends_on = [null_resource.db_hosts_string_1]
}
resource "null_resource" "db_hosts_string_3" {
  count      = var.db_hosts_number > 1 ? 1 : 0
  provisioner "local-exec" {
    command  = "echo [db_slave] >> ${var.hosts_path}"
  }
  depends_on = [null_resource.db_hosts_string_2]
}
resource "null_resource" "db_hosts_string_4" {
  count     = var.db_hosts_number - 1
  provisioner "local-exec" {
    command = "echo ${yandex_compute_instance.db_hosts[count.index+1].name} >> ${var.hosts_path}"
  }
  depends_on = [null_resource.db_hosts_string_3]
}