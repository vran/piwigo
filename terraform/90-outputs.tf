output "web_external_ips" {
  value = join(",",[for host in yandex_compute_instance.web_hosts : host.network_interface.0.nat_ip_address])
}
output "db_external_ips" {
  value = [for host in yandex_compute_instance.db_hosts : host.network_interface.0.nat_ip_address]
}
output "loadbalancer_external_ips" {
  value = [for listeners in yandex_lb_network_load_balancer.loadbalancer_http.listener :
            [for address_specs in listeners.external_address_spec : address_specs.address ]
          ]
}