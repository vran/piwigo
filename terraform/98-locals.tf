locals {
  ssh_key_pub = file("${var.ssh_key_pub_file}")
  ssh_key     = file("${var.ssh_key_file}")
}