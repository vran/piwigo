# provider
variable "sa_key_file" {
  description = "name of service account key file"
  type = string
  default = "key.json"
}
variable "cloud_id" {
  description = "cloud id"
  type = string
  default = "xxxxxxxxxxxxxxxxxxxx"
}
variable "folder_id" {
  description = "cloud folder id"
  type = string
  default = "xxxxxxxxxxxxxxxxxxxx"
}

# sa
variable "sa_name" {
  description = "name of your service account"
  type = string
  default = "sa"
}

# storage
variable "tfstate_bucket_name" {
  description = "s3 bucket name for tfstate"
  type = string
  default = "s3-tfstate"
}
variable "db_filesystem_name" {
  description = "db storage name prefix"
  type = string
  default = "db-filesystem"
}
variable "db_filesystem_type" {
  description = "db storage type"
  type = string
  default = "network-hdd"
}
variable "db_filesystem_size" {
  description = "db storage size in Gb"
  type = number
  default = 10
}

# networks
variable "network_a_name" {
  description = "name of network"
  type = string
  default = "network-a"
}
variable "subnet_a_name" {
  description = "name of subnet"
  type = string
  default = "subnet-a"
}
variable "cidr_subnet_a" {
  description = "subnet address"
  type = string
  default = "172.16.0.0/24"
}
variable "availibility_zone_a" {
  description = "availibility zones: ru-central1-a, ru-central1-b, ru-central1-c (will be disabled)"
  type = string
  default = "ru-central1-a"
}
variable "region_var" {
  description = "there is only one region. you don't need to change this variable"
  type = string
  default = "ru-central1"
}

# security groups
variable "application_port" {
  description = "application port"
  type = number
  default = 80
}
variable "monitoring_ports" {
  description = "ports to be opened for monitoring"
  type = list(number)
  default = [9100, 9104, 9113]
}
variable "whitelist_ip" {
  description = "ip addresses for which the application can be accessed"
  type = list(string)
  default = ["0.0.0.0/0"]
}
variable "management_ip" {
  description = "ip addresses of management host"
  type = string
  default = "169.254.1.1/32"
}
variable "management_port_internal" {
  description = "ssh port for management (internal)"
  type = number
  default = 22
}
variable "management_port_external" {
  description = "ssh port for management (external)"
  type = number
  default = 22
}

# general virtual machine settings
variable "platform_id_name" {
  description = "id of os distributive"
  type = string
  default = "standard-v1"
}
variable "operating_system_id" {
  description = "id of os distributive"
  type = string
  default = "fd87e3vsemiab8q1tl0h"
}
# settings for database hosts
variable "db_hosts_number" {
  description = "number of database servers"
  type = number
  default = 2
}
variable "db_hosts_ram" {
  description = "db server ram size in Gb"
  type = number
  default = 4
}
variable "db_hosts_cpu" {
  description = "number of cpu cores for db server"
  type = number
  default = 2
}
variable "db_hosts_core_fraction" {
  description = "core fraction of cpu for db server"
  type = number
  default = 20
}
variable "db_hosts_disk_size" {
  description = "db server disk size in Gb"
  type = number
  default = 20
}
# settings for web servers
variable "web_hosts_number" {
  description = "number of web (application) servers"
  type = number
  default = 1
}
variable "web_hosts_ram" {
  description = "web hosts ram size in Gb"
  type = number
  default = 4
}
variable "web_hosts_cpu" {
  description = "number of cpu cores for web hosts"
  type = number
  default = 2
}
variable "web_hosts_core_fraction" {
  description = "core fraction of cpu for web hosts"
  type = number
  default = 20
}
variable "web_hosts_disk_size" {
  description = "web hosts disk size in Gb"
  type = number
  default = 20
}

variable "ssh_user" {
  description = "user name for ssh connections"
  type = string
  default = "vagrant"
#   default = "ssh_user"
}
variable "ssh_key_file" {
  description = "path to private ssh key file"
  type = string
  default = "../.ssh/id_ed25519"
}
variable "ssh_key_pub_file" {
  description = "path to public ssh key file"
  type = string
  default = "../.ssh/id_ed25519.pub"
}
variable "hosts_path" {
  description = "path to hosts file for ansible use"
  type = string
  default = "../inventories/prod/hosts_cloud"
}

# loadbalancer
variable "target_group_http_name" {
  description = "target group name for http"
  type = string
  default = "http-targets"
}

variable "lodbalancer_http_name" {
  description = "name of load balancer for http"
  type = string
  default = "loadbalancer-http"
}
variable "lodbalancer_http_deletion_protection" {
  description = "deletion protection of load balancer for http: true or false"
  type = bool
  default = false
}
variable "lodbalancer_http_listener_name" {
  description = "name of load balancer listener for http"
  type = string
  default = "loadbalancer-listener-http"
}
variable "lodbalancer_http_ip_version" {
  description = "ip address version: ipv4 or ipv6"
  type = string
  default = "ipv4"
}
variable "lodbalancer_http_healthcheck_name" {
  description = "name of load balancer health check for http"
  type = string
  default = "http-health"
}
variable "lodbalancer_http_health_interval" {
  description = "interval between health checks, seconds"
  type = number
  default = 2
}
variable "lodbalancer_http_health_timeout" {
  description = "timeout for a target to return a response for the health check, seconds"
  type = number
  default = 1
}
variable "lodbalancer_http_unhealthy_threshold" {
  description = "number of failed health checks before changing the status to unhealthy"
  type = number
  default = 2
}
variable "lodbalancer_http_healthy_threshold" {
  description = "number of successful health checks required in order to set the healthy"
  type = number
  default = 2
}
variable "lodbalancer_http_health_path" {
  description = "url path to set for health checking requests"
  type = string
  default = "/piwigo"
}