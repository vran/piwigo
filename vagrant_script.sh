#!/bin/bash

apt-get update
apt-get install mc -y
if [ $(hostname) = "ansible01" ]; then
    apt-get install python3-pip libssl-dev rsync git curl ansible -y
    mkdir /etc/ansible
    cp /vagrant/ansible.cfg /etc/ansible/ansible.cfg
    pip3 install --upgrade pip
    pip3 install -U git+https://github.com/ansible-community/molecule --break-system-packages
    pip3 install requests pytest-testinfra "molecule[docker]" molecule-docker --break-system-packages
    pip3 install --upgrade molecule==5.1.0 --break-system-packages
    cp -r /vagrant/.ssh/id_ed25519* /home/vagrant/.ssh
    chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519
    chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519.pub
    chmod 600 /home/vagrant/.ssh/id_ed25519
    chmod 644 /home/vagrant/.ssh/id_ed25519.pub
    ansible-galaxy install -r /vagrant/ansible/requirements.yml
    ansible-playbook /vagrant/ansible.yml -i localhost;
else
    cat /vagrant/.ssh/id_ed25519.pub >> /home/vagrant/.ssh/authorized_keys
    cat /vagrant/.ssh/ansible_key.pub >> /home/vagrant/.ssh/authorized_keys;
fi